
(setq  load-path
       (cons  "~/Settings/Emacs"  load-path))

(setq  my-settings-editor  "cygwin-emacs")

(load  "EmacsSettings.el")
